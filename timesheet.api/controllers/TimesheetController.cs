﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/timesheet")]
    [ApiController]
    public class TimesheetController : ControllerBase
    {

        private readonly ITimesheetService timesheetService;
        public TimesheetController(ITimesheetService timesheetService)
        {
            this.timesheetService = timesheetService;
        }

        [HttpGet("get")]
        public IActionResult GetAll([FromQuery]int employeeId , [FromQuery]int weekNo)
        {
            var items = this.timesheetService.GetTimesheetsView(employeeId , weekNo);
            return new ObjectResult(items);
        }

        [HttpPost("add")]
        public IActionResult AddHours([FromBody]Timesheet timesheet)
        {
            var items = this.timesheetService.AddTimesheet(timesheet);
            return new ObjectResult(items);
        }
    }


}