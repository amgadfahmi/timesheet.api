﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class TimesheetService : ITimesheetService
    {
        public TimesheetDb db { get; }
        public TimesheetService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IQueryable<Timesheet> GetTimesheets()
        {
            return this.db.Timesheets;
        }

        public object GetTimesheetsView(int employeeId, int weekNo)
        {
            var moreThanDate = DateTime.Now.AddDays(-(weekNo * 7));
            var lessThanDate = DateTime.Now.AddDays(-(weekNo - 1) * 7);
            var result = from sheets in this.db.Timesheets
                         where sheets.EmployeeId == employeeId
                         && sheets.TaskDate >= moreThanDate
                         && sheets.TaskDate <= lessThanDate
                         group sheets by new { sheets.TaskId } into temp
                         join emp in this.db.Employees on temp.FirstOrDefault().EmployeeId equals emp.Id
                         join task in this.db.Tasks on temp.FirstOrDefault().TaskId equals task.Id
                         orderby task.Name
                        
                         select new
                         {
                             EmployeeName = emp.Name,
                             TaskName = task.Name,
                             Saturday = GetDayValue(temp, DayOfWeek.Saturday),
                             Sunday = GetDayValue(temp, DayOfWeek.Sunday),
                             Monday = GetDayValue(temp, DayOfWeek.Monday),
                             Tuesday = GetDayValue(temp, DayOfWeek.Tuesday),
                             Wednesday = GetDayValue(temp, DayOfWeek.Wednesday),
                             Thursday = GetDayValue(temp, DayOfWeek.Thursday),
                             Friday = GetDayValue(temp, DayOfWeek.Friday)
                         };


            return result;
        }

        private int GetDayValue(IGrouping<object, Timesheet> temp, DayOfWeek compareTo)
        {
            return temp.Where(x => x.TaskDate.DayOfWeek == compareTo).Sum(x => x.Hours);
        }

        public bool AddTimesheet(Timesheet timesheet)
        {
            this.db.Timesheets.Add(timesheet);
            return this.db.SaveChanges() > 0;
        }
    }
}
