﻿using System;
using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{

    public class EmployeeService : IEmployeeService
    {
        public TimesheetDb db { get; }
        public EmployeeService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IQueryable<Employee> GetEmployees()
        {
            return this.db.Employees;
        }

        public IQueryable<object> GetEmployeesDetails()
        {
            var result = from emp in this.db.Employees
                         join sheets in this.db.Timesheets on emp.Id equals sheets.EmployeeId into sheetsTB
                         from sheets in sheetsTB.DefaultIfEmpty()
                         where sheets.TaskDate > DateTime.Now.AddDays(-7) || sheets.TaskDate == null
                         group sheets by new { emp.Id, emp.Code, emp.Name } into temp
                         select new
                         {
                             Id = temp.Key.Id,
                             Code = temp.Key.Code,
                             Name = temp.Key.Name,
                             Total = temp.Sum(x => x != null ? x.Hours : 0),
                             Average = Math.Round(temp.Average(x => x != null ? x.Hours : 0), 1)
                         };

            return result;
        }
    }
}
