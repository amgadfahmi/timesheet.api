﻿using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public interface ITaskService
    {
        TimesheetDb db { get; }

        IQueryable<Task> GetTasks();

    }
}