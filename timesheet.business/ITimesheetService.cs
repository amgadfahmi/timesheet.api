﻿using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public interface ITimesheetService
    {
        TimesheetDb db { get; }

        IQueryable<Timesheet> GetTimesheets();
        object GetTimesheetsView(int employeeId, int weekNo);
        bool AddTimesheet(Timesheet timesheet);
    }
}