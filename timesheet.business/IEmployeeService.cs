﻿using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public interface IEmployeeService
    {
        TimesheetDb db { get; }

        IQueryable<Employee> GetEmployees();
        IQueryable<object> GetEmployeesDetails();
   
    }
}